import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class PrintmoveThread extends Thread
{
    private Car car;
    private Highway way;
    private JLabel l;
    private Action action;
    private int carindex;
    private double updatespeed;
    public PrintmoveThread(Car car,Highway way,JLabel l,Action action,double updatespeed)
    {
    	 this.car=car;
         this.way=way;
         this.l=l;
         this.action=action;
         this.updatespeed=updatespeed;
    }
    public void run() 
    { 
        while(true)
        {
            if(action.num==2||action.num==0)
            {
                delay(100);
                continue;
            }
            if(action.num==3||!way.move(car,0))
                return;
            car.modify(way.getdis(car.getlocation(),car.gety()));
            delay((int)(updatespeed*1000));
        }
    }
    private void delay(int x)
    {
        try 
        { 
            Thread.sleep(x); 
        } 
        catch(InterruptedException e) 
        { 
            System.out.println("I am interrupted...."); 
        } 
    }
} 
