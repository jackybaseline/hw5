import java.applet.Applet;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class MyButton extends JButton
{
	public MyButton()
    {
        setFont(new Font("", Font.PLAIN, 15));
        setBounds(0,0,90,30);
    }
}
abstract class MyButtonListener implements ActionListener
{
    protected Action action;
    public MyButtonListener(Action action)
    {
        this.action=new Action();
        this.action=action;
    }
}

class StartButton extends MyButton
{
    public StartButton(Action action)
    {
       setText("Start");
       setLocation(40,20);
       addActionListener(new StartButtonListener(action));
    }
    private class StartButtonListener extends MyButtonListener
    {
        public StartButtonListener (Action action)
        {
            super(action);
        }
        public void actionPerformed(ActionEvent e)
        {
            action.num=1;
        }
    }
}

class StopButton extends MyButton
{
    public StopButton(Action action)
    {
       setText("Stop");
       setLocation(150,20);
       addActionListener(new StopButtonListener(action));
    }
    private class StopButtonListener extends MyButtonListener
    {
        public StopButtonListener(Action action)
        {
            super(action);
        }
        public void actionPerformed(ActionEvent e)
        {
            action.num=2;
        }
    }
}

class RestartButton extends MyButton
{
    public RestartButton(Action action)
    {
       setText("Restart");
       setLocation(260,20);
       addActionListener(new RestartButtonListener(action));
    }
    private class RestartButtonListener extends MyButtonListener
    {
        public RestartButtonListener(Action action)
        {
            super(action);
        }
        public void actionPerformed(ActionEvent e)
        {
            action.num=3;
        }
    }
}