import java.applet.Applet;
import java.awt.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class CarSimulation extends JApplet implements KeyEventDispatcher
{
  private Action action;
  private CarAction caraction;
  private Car car[];
  private Highway way;
  public CarSimulation()
  {
      action=new Action();
      caraction=new CarAction();
  }
  public void init() 
  { 
    KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
  }
  public void start()
  {
    getContentPane().setLayout(null);

    JLabel speedmax_l=new JLabel("MaxSpeed");
    speedmax_l.setFont(new Font("", Font.BOLD, 15));
    speedmax_l.setBounds(350,20,80,30);
    getContentPane().add(speedmax_l);
    JTextField speedmax_text=new JTextField("10");
    speedmax_text.setFont(new Font("", Font.PLAIN, 16));
    speedmax_text.setBounds(440,20,65,30);
    getContentPane().add(speedmax_text);

    JLabel carmax_l=new JLabel("MaxCar");
    carmax_l.setFont(new Font("", Font.BOLD, 15));
    carmax_l.setBounds(520,20,70,30);
    getContentPane().add(carmax_l);
    JTextField carmax_text=new JTextField("20");
    carmax_text.setFont(new Font("", Font.PLAIN, 16));
    carmax_text.setBounds(590,20,70,30);
    getContentPane().add(carmax_text);

    JLabel lengthmax_l=new JLabel("Length");
    lengthmax_l.setFont(new Font("", Font.BOLD, 15));
    lengthmax_l.setBounds(670,20,50,30);
    getContentPane().add(lengthmax_l);
    JTextField lengthmax_text=new JTextField("1000");
    lengthmax_text.setFont(new Font("", Font.PLAIN, 16));
    lengthmax_text.setBounds(730,20,70,30);
    getContentPane().add(lengthmax_text);

    JLabel highwaymax_l=new JLabel("MaxHighway");
    highwaymax_l.setFont(new Font("", Font.BOLD, 15));
    highwaymax_l.setBounds(350,70,90,30);
    getContentPane().add(highwaymax_l);
    JTextField highwaymax_text=new JTextField("3");
    highwaymax_text.setFont(new Font("", Font.PLAIN, 16));
    highwaymax_text.setBounds(450,70,60,30);
    getContentPane().add(highwaymax_text);

    JLabel updatespeed_l=new JLabel("Updateinterval");
    updatespeed_l.setFont(new Font("", Font.BOLD, 15));
    updatespeed_l.setBounds(520,70,110,30);
    getContentPane().add(updatespeed_l);
    JTextField updatespeed_text=new JTextField("0.1");
    updatespeed_text.setFont(new Font("", Font.PLAIN, 16));
    updatespeed_text.setBounds(640,70,70,30);
    getContentPane().add(updatespeed_text);



    StartButton start_b=new StartButton(action);
    getContentPane().add(start_b);

    StopButton stop_b=new StopButton(action);
    getContentPane().add(stop_b);

    RestartButton restart_b=new RestartButton(action);
    getContentPane().add(restart_b);


    getContentPane().repaint();

    int highway_len=1000;
    int carmax=50;
    int speedmax=10;
    int highwaymax=3;
    double updatespeed=0.1;
    car=new Car[carmax];
    way= new Highway(highway_len,speedmax);
    JLabel l=new JLabel("0");
    l.setBounds(0,0,50,50);
    getContentPane().add(l);
    PrintmoveThread thread[]=new PrintmoveThread[carmax];

    while(true)
    { 
      if(action.num==3||action.num==1)
      {
        speedmax=Integer.parseInt(speedmax_text.getText());
        carmax=Integer.parseInt(carmax_text.getText());
        highway_len=Integer.parseInt(lengthmax_text.getText());
        highwaymax=Integer.parseInt(highwaymax_text.getText());
        updatespeed=Double.parseDouble(updatespeed_text.getText());
        way= new Highway(highway_len,speedmax);
        car=new Car[carmax];
        for(int i=0;i<carmax;i++)
        {  
          car[i]=new Car(0,i,speedmax,highwaymax,caraction,l);
          getContentPane().add(car[i].getmove());
          thread[i]=new PrintmoveThread(car[i],way,l,action,updatespeed);
        }
        for(int i=0;i<carmax;++i)
          thread[i].start();

        for(int i=0;i<carmax;++i)
        {
          try 
          {
            thread[i].join();
            getContentPane().remove(car[i].getmove());
            getContentPane().repaint();
            car[i]=null;
          } 
          catch(InterruptedException e1) 
          { 
            e1.printStackTrace(); 
          }
        }
        action.num=1;
        caraction.num=0;
        l.setText(""+caraction.num);
      }
      delay(100);
    }

  }
  public void stop() 
  { 

  }

  public void paint(Graphics g) 
  {

  }
  public boolean dispatchKeyEvent(KeyEvent e)
  { 
    Car nowcar=car[caraction.num];
    switch(e.getKeyCode())
    {
      case KeyEvent.VK_UP:
        if(way.check(nowcar,nowcar.getlocation(),nowcar.gety()-20,false,true)  )
        {
          way.now[nowcar.gety()][nowcar.getlocation()]=false;
          nowcar.getmove().setLocation(nowcar.getlocation(),nowcar.gety()-20);
          nowcar.sety(nowcar.gety()-20);
          way.now[nowcar.gety()][nowcar.getlocation()]=true;
        }
        break;
      case KeyEvent.VK_DOWN:
        if(way.check(nowcar,nowcar.getlocation(),nowcar.gety()+20,false,false)  )
        { 
          way.now[nowcar.gety()][nowcar.getlocation()]=false;
          nowcar.getmove().setLocation(nowcar.getlocation(),nowcar.gety()+20);
          nowcar.sety(nowcar.gety()+20);
          way.now[nowcar.gety()][nowcar.getlocation()]=true;
        }
        break;
      case KeyEvent.VK_LEFT:
        if(way.check(nowcar,nowcar.getlocation()-5,nowcar.gety(),true,false)  )
        {
          way.now[nowcar.gety()][nowcar.getlocation()]=false;
          nowcar.getmove().setLocation(nowcar.getlocation()-5,nowcar.gety());
          nowcar.setlocation(nowcar.getlocation()-5);
          way.now[nowcar.gety()][nowcar.getlocation()]=true;
        }
        break;
      case KeyEvent.VK_RIGHT:
        if(way.check(nowcar,nowcar.getlocation()+5,nowcar.gety(),true,true)  )
        {
          way.now[nowcar.gety()][nowcar.getlocation()]=false;
          nowcar.getmove().setLocation(nowcar.getlocation()+5,nowcar.gety());
          nowcar.setlocation(nowcar.getlocation()+5);
          way.now[nowcar.gety()][nowcar.getlocation()]=true;
        }
        break;
    }
    getContentPane().repaint();
    return false;
  }
  private void delay(int x)
  {
      try 
      { 
          Thread.sleep(x); 
      } 
      catch(InterruptedException e) 
      { 
          System.out.println("I am interrupted...."); 
      } 
  }
}
class Action
{
  public int num;
  public Action()
  {
    num=0;
  }
}
class CarAction
{
  public int num;
  public CarAction()
  {
    num=0;
  }
} 