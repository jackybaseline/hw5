import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class Highway
{	
	int length;
	boolean[][] now;
	private int speedmax;
	public Highway(int length,int speedmax)
	{
		this.length=length;
		this.speedmax=speedmax;
		now=new boolean[1000][];
		for(int i=0;i<length;++i)
			now[i]=new boolean[length];
	}

	/** get the distance from the front of the car */
	public int getdis(int location,int thiscar_y)                   
	{
		for(int i=location+1;i<length&&i<location+2*(speedmax)+50;i++)
		{
			if(now[thiscar_y][i]||now[thiscar_y+20][i]||now[thiscar_y-20][i])
				return i-location-50;
		}
			return 2*(speedmax);
	}
	 /**move this car by its location*/
	public synchronized boolean move(Car car,int start)           
	{
		if(car.getlocation()+car.getspeed()>=length)
		{
			now[car.gety()][car.getlocation()]=false;
			return false;
		}
		else if(now[car.gety()][car.getlocation()+car.getspeed()])
			return true;
		now[car.gety()][car.getlocation()]=false;
		car.setlocation(car.getlocation()+car.getspeed());
		car.getmove().setLocation(car.getlocation(),car.gety());
		now[car.gety()][car.getlocation()]=true;
		return true;
	}
	public boolean check(Car car,int x,int y,boolean isX,boolean positive)           
	{
		if(isX)
		{
			if(positive)
			{
				for(int i=0;i<50;++i)
				{
					if(x+i>=length||now[y][x+i]||now[y+20][x+i]||now[y-20][x+i])
						return false;
				}
				return true;
			}
			else
			{
				for(int i=0;i<50;++i)
				{
					if(x-i<0||now[y][x-i]||now[y+20][x-i]||now[y-20][x-i])
						return false;
				}
				return true;
			}
		}
		else
		{
			if(positive)
			{
				for(int i=0;i<100;++i)
				{
					if(x-50+i<0||x-50+i>length||now[y][x-50+i]||now[y-20][x-50+i])
						return false;
				}
				return true;
			}
			else
			{
				for(int i=0;i<100;++i)
				{
					if(x-50+i<0||x-50+i>length||now[y][x-50+i]||now[y+20][x-50+i])
						return false;
				}
				return true;
			}
		}
	}
}