import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;
public class Car
{
  private int speedmax;
  private int location;
  private int speed;
  private int acceleratespeed;
  private int slowspeed;
  private int x;
  private int y;
  private int accelerating;     //store this car accelerating time  
  private boolean slowing;  //store this car slowing or not 
  private JButton move;
  public Car(int start,int i,int speedmax,int road,CarAction caraction,JLabel l)
  {
    Random ran = new Random();
    this.speedmax=speedmax;
    location=start;
    y=200+(i%road)*40;
    speed=speedmax;
    String str="src/pic/car"+ran.nextInt(16)+".gif";
    ImageIcon image=new ImageIcon(this.getClass().getClassLoader().getResource((str)));
    move=new JButton(image);
    move.setBounds(0,y,50,35);
    move.addActionListener(new CarButtonListener(caraction,i,l));
    x=0;
  }
  public int getspeed()
  {
    return speed;
  }
  public JButton getmove()
  {
    return move;
  }
  public void setlocation(int location)
  {
    this.location=location;
  }
  public int getspeedmax()
  {
    return speedmax;
  }
  public int getlocation()
  {
    return location;
  }
  public int gety()
  {
    return y;
  }
  public void setx(int change)
  {
    x=change;
  }
  public void sety(int change)
  {
    y=change;
  }
    /** the cars which are on the highway get its speed function */
  public void modify(int limit)   
  { 
    if(slowing==true)                  /**   slow on time  "t+1"   */
    {
      speed=slowspeed;
      slowing=false;
      return;
    }
    else if(accelerating==1)              /**quick on time "t+1"*/
    {
      accelerating=2;
      return ;
    }
    else if(accelerating==2)              /**quick on time "t+2"*/
    {
      speed=acceleratespeed;
      accelerating=0;
      return; 
    }
    int tempspeed;
    if(limit/2>speedmax)
      tempspeed=speedmax;
    else if(limit/2<1)
      tempspeed=0;
    else
      tempspeed=limit/2;
    if(speed>tempspeed)                     /**   slow on time  "t"   */
    {
      slowspeed=tempspeed;
      slowing=true;
    }
    else if(speed<tempspeed)                /**quick on time "t"*/
    {
      acceleratespeed=tempspeed;
      accelerating=1;
    }
  }
    private class CarButtonListener implements ActionListener
    {
        private CarAction caraction;
        private int index;
        private JLabel l;
        public CarButtonListener(CarAction caraction,int index,JLabel l)
        {
          this.caraction=caraction;
          this.index=index;
          this.l=new JLabel();
          this.l=l;
        }
        public void actionPerformed(ActionEvent e)
        {
            caraction.num=index;
            l.setText(""+(caraction.num+1));
        }
    }
}